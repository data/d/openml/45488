# OpenML dataset: timing-attack-dataset-15-micro-seconds-delay-2022-09-09

https://www.openml.org/d/45488

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bleichenbacher Timing Attack: 15 micro seconds dataset created on 2022-09-09
Attribute Descriptions:

CCS0:tcp.srcport: TCP Source Port of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.dstport: TCP Destination Port of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.port: TCP Source or Destination Port of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.stream: TCP Stream index of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.len: TCP Segment Len of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.seq: TCP Sequence Number of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.nxtseq: TCP Next Sequence Number of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.ack: TCP Acknowledgment Number of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.hdr_len: TCP 1000 .... = Header Length of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.res: TCP 000. .... .... = Reserved of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.ns: TCP ...0 .... .... = Nonce of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.urg: TCP .... ..0. .... = Urgent of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.push: TCP .... .... 0... = Push of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.reset: TCP .... .... .0.. = Reset of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.syn: TCP .... .... ..0. = Syn of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.flags.fin: TCP .... .... ...0 = Fin of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.window_size_value: TCP Window of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.window_size: TCP Calculated window size of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.window_size_scalefactor: TCP Window size scaling factor of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.checksum.status: TCP Checksum Status of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.urgent_pointer: TCP Urgent Pointer of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.options.nop: TCP tcp.options.nop of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.option_kind: TCP Kind of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.option_len: TCP Length of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.options.timestamp.tsval: TCP Timestamp value of the first Change Cipher Spec TCP Acknowledgement
CCS0:tcp.time_delta: Time since previous frame in this TCP stream of the first Change Cipher Spec TCP Acknowledgement
CCS0:order: Message order of the first Change Cipher Spec TCP Acknowledgement within the server responses
TLS0:tcp.srcport: TCP Source Port of the first TLS Alert
TLS0:tcp.dstport: TCP Destination Port of the first TLS Alert
TLS0:tcp.port: TCP Source or Destination Port of the first TLS Alert
TLS0:tcp.stream: TCP Stream index of the first TLS Alert
TLS0:tcp.len: TCP Segment Len of the first TLS Alert
TLS0:tcp.seq: TCP Sequence Number of the first TLS Alert
TLS0:tcp.nxtseq: TCP Next Sequence Number of the first TLS Alert
TLS0:tcp.ack: TCP Acknowledgment Number of the first TLS Alert
TLS0:tcp.hdr_len: TCP 1000 .... = Header Length of the first TLS Alert
TLS0:tcp.flags.res: TCP 000. .... .... = Reserved of the first TLS Alert
TLS0:tcp.flags.ns: TCP ...0 .... .... = Nonce of the first TLS Alert
TLS0:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the first TLS Alert
TLS0:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the first TLS Alert
TLS0:tcp.flags.urg: TCP .... ..0. .... = Urgent of the first TLS Alert
TLS0:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the first TLS Alert
TLS0:tcp.flags.push: TCP .... .... 1... = Push of the first TLS Alert
TLS0:tcp.flags.reset: TCP .... .... .0.. = Reset of the first TLS Alert
TLS0:tcp.flags.syn: TCP .... .... ..0. = Syn of the first TLS Alert
TLS0:tcp.flags.fin: TCP .... .... ...0 = Fin of the first TLS Alert
TLS0:tcp.window_size_value: TCP Window of the first TLS Alert
TLS0:tcp.window_size: TCP Calculated window size of the first TLS Alert
TLS0:tcp.window_size_scalefactor: TCP Window size scaling factor of the first TLS Alert
TLS0:tcp.checksum.status: TCP Checksum Status of the first TLS Alert
TLS0:tcp.urgent_pointer: TCP Urgent Pointer of the first TLS Alert
TLS0:tcp.options.nop: TCP tcp.options.nop of the first TLS Alert
TLS0:tcp.option_kind: TCP Kind of the first TLS Alert
TLS0:tcp.option_len: TCP Length of the first TLS Alert
TLS0:tcp.options.timestamp.tsval: TCP Timestamp value of the first TLS Alert
TLS0:tcp.time_delta: Time since previous frame in this TCP stream of the first TLS Alert
TLS0:tls.record.content_type: TLS Content Type of the first TLS Alert
TLS0:tls.record.length: TLS Length of the first TLS Alert
TLS0:tls.alert_message.level: TLS Level of the first TLS Alert
TLS0:tls.alert_message.desc: TLS Description of the first TLS Alert
TLS0:order: Message order of the first TLS Alert within the server responses
DISC0:tcp.srcport: TCP Source Port of the first TCP Disconnect
DISC0:tcp.dstport: TCP Destination Port of the first TCP Disconnect
DISC0:tcp.port: TCP Source or Destination Port of the first TCP Disconnect
DISC0:tcp.stream: TCP Stream index of the first TCP Disconnect
DISC0:tcp.len: TCP Segment Len of the first TCP Disconnect
DISC0:tcp.seq: TCP Sequence Number of the first TCP Disconnect
DISC0:tcp.nxtseq: TCP Next Sequence Number of the first TCP Disconnect
DISC0:tcp.ack: TCP Acknowledgment Number of the first TCP Disconnect
DISC0:tcp.hdr_len: TCP 1000 .... = Header Length of the first TCP Disconnect
DISC0:tcp.flags.res: TCP 000. .... .... = Reserved of the first TCP Disconnect
DISC0:tcp.flags.ns: TCP ...0 .... .... = Nonce of the first TCP Disconnect
DISC0:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the first TCP Disconnect
DISC0:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the first TCP Disconnect
DISC0:tcp.flags.urg: TCP .... ..0. .... = Urgent of the first TCP Disconnect
DISC0:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the first TCP Disconnect
DISC0:tcp.flags.push: TCP .... .... 0... = Push of the first TCP Disconnect
DISC0:tcp.flags.reset: TCP .... .... .0.. = Reset of the first TCP Disconnect
DISC0:tcp.flags.syn: TCP .... .... ..0. = Syn of the first TCP Disconnect
DISC0:tcp.flags.fin: TCP .... .... ...1 = Fin of the first TCP Disconnect
DISC0:tcp.window_size_value: TCP Window of the first TCP Disconnect
DISC0:tcp.window_size: TCP Calculated window size of the first TCP Disconnect
DISC0:tcp.window_size_scalefactor: TCP Window size scaling factor of the first TCP Disconnect
DISC0:tcp.checksum.status: TCP Checksum Status of the first TCP Disconnect
DISC0:tcp.urgent_pointer: TCP Urgent Pointer of the first TCP Disconnect
DISC0:tcp.options.nop: TCP tcp.options.nop of the first TCP Disconnect
DISC0:tcp.option_kind: TCP Kind of the first TCP Disconnect
DISC0:tcp.option_len: TCP Length of the first TCP Disconnect
DISC0:tcp.options.timestamp.tsval: TCP Timestamp value of the first TCP Disconnect
DISC0:tcp.time_delta: Time since previous frame in this TCP stream of the first TCP Disconnect
DISC0:order: Message order of the first TCP Disconnect within the server responses
DISC1:tcp.srcport: TCP Source Port of the second TCP Disconnect
DISC1:tcp.dstport: TCP Destination Port of the second TCP Disconnect
DISC1:tcp.port: TCP Source or Destination Port of the second TCP Disconnect
DISC1:tcp.stream: TCP Stream index of the second TCP Disconnect
DISC1:tcp.len: TCP Segment Len of the second TCP Disconnect
DISC1:tcp.seq: TCP Sequence Number of the second TCP Disconnect
DISC1:tcp.nxtseq: TCP Next Sequence Number of the second TCP Disconnect
DISC1:tcp.ack: TCP Acknowledgment Number of the second TCP Disconnect
DISC1:tcp.hdr_len: TCP 1000 .... = Header Length of the second TCP Disconnect
DISC1:tcp.flags.res: TCP 000. .... .... = Reserved of the second TCP Disconnect
DISC1:tcp.flags.ns: TCP ...0 .... .... = Nonce of the second TCP Disconnect
DISC1:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the second TCP Disconnect
DISC1:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the second TCP Disconnect
DISC1:tcp.flags.urg: TCP .... ..0. .... = Urgent of the second TCP Disconnect
DISC1:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the second TCP Disconnect
DISC1:tcp.flags.push: TCP .... .... 0... = Push of the second TCP Disconnect
DISC1:tcp.flags.reset: TCP .... .... .0.. = Reset of the second TCP Disconnect
DISC1:tcp.flags.syn: TCP .... .... ..0. = Syn of the second TCP Disconnect
DISC1:tcp.flags.fin: TCP .... .... ...0 = Fin of the second TCP Disconnect
DISC1:tcp.window_size_value: TCP Window of the second TCP Disconnect
DISC1:tcp.window_size: TCP Calculated window size of the second TCP Disconnect
DISC1:tcp.window_size_scalefactor: TCP Window size scaling factor of the second TCP Disconnect
DISC1:tcp.checksum.status: TCP Checksum Status of the second TCP Disconnect
DISC1:tcp.urgent_pointer: TCP Urgent Pointer of the second TCP Disconnect
DISC1:tcp.options.nop: TCP tcp.options.nop of the second TCP Disconnect
DISC1:tcp.option_kind: TCP Kind of the second TCP Disconnect
DISC1:tcp.option_len: TCP Length of the second TCP Disconnect
DISC1:tcp.options.timestamp.tsval: TCP Timestamp value of the second TCP Disconnect
DISC1:tcp.time_delta: Time since previous frame in this TCP stream of the second TCP Disconnect
DISC1:order: Message order of the second TCP Disconnect within the server responses

fold_id 2
vulnerable_classes [0x00_in_PKCS#1_padding_(first_8_bytes_after_0x00_0x02), Correctly_formatted_PKCS#1_PMS_message__but_1_byte_shorter, No_0x00_in_message, Wrong_first_byte_(0x00_set_to_0x17), Wrong_second_byte_(0x02_set_to_0x17)]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45488) of an [OpenML dataset](https://www.openml.org/d/45488). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45488/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45488/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45488/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

